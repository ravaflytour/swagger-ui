export default {

  browserPortDev: 3003,
  UIPort: 3001,
  testPort: 3002,

  sourceDir: './src/infra/client/',
  buildDir: './build/client',

  styles: {
    src: 'src/infra/client/styles/**/*.scss',
    dest: 'build/client/css',
    prodSourcemap: false,
    sassIncludePaths: []
  },

  scripts: {
    src: 'src/infra/client/js/**/*.js',
    server: 'src/infra/server/**/*.js',
    destClient: 'build/client/js',
    destServer: 'build/server',
    mainServer: 'build/server/app.js',
    test: 'test/**/*.js',
    gulp: 'gulp/**/*.js'
  },

  images: {
    src: 'src/infra/client/images/**/*',
    dest: 'build/client/images'
  },

  fonts: {
    src: ['src/infra/client/fonts/**/*'],
    dest: 'build/client/fonts'
  },

  assetExtensions: [
    'js',
    'css',
    'png',
    'jpe?g',
    'gif',
    'svg',
    'eot',
    'otf',
    'ttc',
    'ttf',
    'woff2?'
  ],

  views: {
    index: 'src/infra/client/index.+(jade|html)',
    src: 'src/infra/client/views/**/*.+(jade|html)',
    dest: 'src/infra/client/js'
  },

  gzip: {
    src: 'build/**/*.{html,xml,json,css,js,js.map,css.map}',
    dest: 'build/client',
    options: {}
  },

  browserify: {
    bundleName: 'main.js',
    prodSourcemap: false
  },

  test: {
    karma: 'test/karma.conf.js',
    protractor: 'test/protractor.conf.js'
  },

  init: function() {
    this.views.watch = [
      this.views.index,
      this.views.src
    ];

    return this;
  }

}.init();
