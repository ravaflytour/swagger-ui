import config      from '../config';
import browserSync from 'browser-sync';
import gulp        from 'gulp';

gulp.task('browserSync', function() {
  browserSync.init(null, {
    proxy: 'http://localhost:' + (process.env.PORT || 3000),
    port: config.browserPortDev,
    ui: {
      port: config.UIPort
    },
    ghostMode: {
      links: false
    }
  });
});
