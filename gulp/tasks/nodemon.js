import gulp from 'gulp';
import config from '../config';
import nodemon from 'gulp-nodemon';

gulp.task('nodemon', () => {
    return nodemon({
      script: config.scripts.mainServer,
      watch: config.scripts.server,
      tasks: ['babel-compile-server']
   });
});
