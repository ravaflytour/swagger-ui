import gulp from 'gulp';
import sourcemaps from'gulp-sourcemaps';
import babel from 'gulp-babel';
import config from '../config';

gulp.task('babel-compile-server', () => {
    return gulp.src(config.scripts.server)
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015', 'stage-1']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.scripts.destServer));
});
