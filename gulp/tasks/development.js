import gulp        from 'gulp';
import runSequence from 'run-sequence';
import dotenv      from 'dotenv';

gulp.task('dev', ['clean'], function(cb) {
  dotenv.config({silent: true});
  global.isProd = false;
  runSequence(['styles', 'images', 'fonts', 'views'], 'browserify', 'babel-compile-server', 'nodemon', 'watch', cb);

});
