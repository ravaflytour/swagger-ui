# Flytour Node.js boilerplate

fork from https://github.com/jakemmarsh/angularjs-gulp-browserify-boilerplate

## Usage

```
npm run dev
```

This will watch the changes in files, compile jade and es6 to build folder and sync with browser (live-reload)

#### test

```
npm test
```

#### build

```
npm run build
```


#### lint

```
npm run lint
```

#### coverage

```
npm run coverage
```
