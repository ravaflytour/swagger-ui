import angular from 'angular';

// angular modules
import constants from './constants';
import onConfig  from './on_config';
import onRun     from './on_run';
import 'angular-ui-router';
import 'angular-sanitize';
import 'angular-highlight';
import 'angular-swagger-ui/dist/scripts/swagger-ui.js';
import 'angular-swagger-ui/dist/scripts/modules/swagger-xml-formatter.min.js';
import './templates';
import './home';
import './swagger';

const slug = constants.appSlug;

// create and bootstrap application
const requires = [
  'ui.router',
  'ngSanitize',
  'swaggerUi',
  'angular-highlight',
  'templates',
  `${slug}.home.controllers`,
  `${slug}.home.directives`,
  `${slug}.home.factories`,
  `${slug}.home.filters`,
  `${slug}.home.services`,
  `${slug}.swagger.controllers`,
  `${slug}.swagger.services`,
  `${slug}.swagger.filters`,
  `${slug}.swagger.directives`
];


//hljs.initHighlightingOnLoad();

// mount on window for testing
window.app = angular.module(slug, requires);

angular.module(slug).constant('AppSettings', constants);
angular.module(slug).config(onConfig);
angular.module(slug).run(onRun);

angular.bootstrap(document, [slug], {
  strictDi: true
});
