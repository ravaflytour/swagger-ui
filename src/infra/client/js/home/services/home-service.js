function ExampleService($http) {
  'ngInject';

  this.get = function() {
    return new Promise((resolve, reject) => {
      $http.get('apiPath').success((data) => {
        resolve(data);
      }).error((err, status) => {
        reject(err, status);
      });
    });
  };

}

export default {
  name: 'ExampleService',
  fn: ExampleService
};
