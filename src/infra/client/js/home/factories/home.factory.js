function ExampleFactory($http) {
  'ngInject';

  const factory = {};

  factory.get = function() {
    return new Promise((resolve, reject) => {
      $http.get('apiPath').success((data) => {
        resolve(data);
      }).error((err, status) => {
        reject(err, status);
      });
    });
  };

  return factory;

}

export default {
  name: 'ExampleFactory',
  fn: ExampleFactory
};
