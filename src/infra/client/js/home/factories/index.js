import angular from 'angular';
import Appsettings from '../../constants';
import path from 'path';

const slug = Appsettings.appSlug;
const moduleName = path.basename(path.resolve(__dirname, '../'));
const bulk = require('bulk-require');
const factoriesModule = angular.module(`${slug}.${moduleName}.factories`, []);
const factories = bulk(__dirname, ['./**/!(*index|*.spec).js']);

function declare(factoryMap) {
  Object.keys(factoryMap).forEach((key) => {
    let item = factoryMap[key];

    if (!item) {
      return;
    }

    if (item.fn && typeof item.fn === 'function') {
      factoriesModule.factory(item.name, item.fn);
    } else {
      declare(item);
    }
  });
}

declare(factories);

export default factoriesModule;
