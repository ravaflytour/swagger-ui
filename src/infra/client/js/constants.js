const AppSettings = {
  appTitle: 'Example Application',
  apiUrl: '/api/v1',
  appSlug: 'app'
};

export default AppSettings;
