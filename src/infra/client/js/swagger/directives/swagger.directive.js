function SwaggerDirective() {
  return {
    restrict: 'E',
    templateUrl: 'swagger/directives/swagger.html',
    scope: '=',
    controller: 'SwaggerCtrl as controller',
    link: (scope, element, attrs, controller) => {
      const url = attrs.url;
      const token = attrs.token;
      scope.token = controller.token = token;
      scope.url = controller.url = url;
    }
  };
}

export default {
  name: 'swagger',
  fn: SwaggerDirective
};
