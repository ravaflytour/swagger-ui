function SwaggerExampleDirective() {
  return {
    restrict: 'E',
    templateUrl: 'swagger/directives/swagger-example/index.html',
    scope: '=',
    controller: 'SwaggerCtrl as controller',
    link: (scope, element, attrs, controller) => {
      scope.token = controller.token;
    }
  };
}

export default {
  name: 'swaggerExample',
  fn: SwaggerExampleDirective
};
