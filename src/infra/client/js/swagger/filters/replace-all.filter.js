function ReplaceAll() {

  return function(input, before, after) {
    const re = new RegExp(before, 'g')
    return input.replace(re, after);
  };

}

export default {
  name: 'replaceAll',
  fn: ReplaceAll
};
