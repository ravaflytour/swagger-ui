import angular from 'angular';
import Appsettings from '../../constants';
import path from 'path';

const slug = Appsettings.appSlug;
const bulk = require('bulk-require');
const moduleName = path.basename(path.resolve(__dirname, '../'));
const filtersModule = angular.module(`${slug}.${moduleName}.filters`, []);
const filters = bulk(__dirname, ['./**/!(*index|*.spec).js']);

function declare(filterMap) {
  Object.keys(filterMap).forEach((key) => {
    let item = filterMap[key];

    if (!item) {
      return;
    }

    if (item.fn && typeof item.fn === 'function') {
      filtersModule.filter(item.name, item.fn);
    } else {
      declare(item);
    }
  });
}

declare(filters);

export default filtersModule;
