function ParseParameter() {

  return function(parameter, language) {
    var template = `${parameter.name}`;
    switch(language) {
    case 'nodejs':
        template = `\`\${${parameter.name}}\``;
        break;
    case 'curl':
        template = `{${parameter.name}}`;
        break;
    case 'csharp':
        template = `\${${parameter.name}}`;
        break;
    }
    return parameter.schema ? parameter.schema.json : template;
  };

}

export default {
  name: 'parseParameter',
  fn: ParseParameter
};

