function SwaggerCtrl($scope, SwaggerOAuthService, swaggerModules) {
  'ngInject';

  const vm = this;

  vm.onError = function onError(message, code) {
    console.log(message, code);
  };

  vm.execute = function execute(request) {
    return SwaggerOAuthService.execute(vm.token, request);
  };

  vm.hasParameterIn = function hasParameterIn(parameterIn, operation) {
//    return vm.getParametersIn(parameterIn, operation).length > 0;
    return operation.parameters.filter(parameter => parameter.in === parameterIn).length > 0;
  };

  vm.getToken = function getToken() {
    return vm.token || $scope.token || '`${token}`';
  };

  vm.getParametersIn = function getParametersIn(parameterIn, operation) {
    return operation.parameters.filter(parameter => parameter.in === parameterIn);
  };

  vm.parseQueryString = function parseQueryString(parameters) {
    const qs = parameters.map(p => `${p.name}={${p.name}}`).join('&');
    return qs.length > 0 ? `?${qs}` : '';
  }
  swaggerModules.add(swaggerModules.BEFORE_EXPLORER_LOAD, vm);

  return vm;
}


export default {
  name: 'SwaggerCtrl',
  fn: SwaggerCtrl
};
