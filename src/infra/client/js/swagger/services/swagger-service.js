function SwaggerOAuthService($q, $http) {
  'ngInject';

  this.get = function () {
    return new Promise((resolve, reject) => {
      $http.get('apiPath').success((data) => {
        resolve(data);
      }).error((err, status) => {
        reject(err, status);
      });
    });
  };

  this.execute = function execute (token, request) {
    var executed = false;
    const deferred = $q.defer();

    if (token) {
      request.headers = request.headers || [];
      request.headers['Authorization'] = `Bearer ${token}`;
      executed = true;
    }

    deferred.resolve(executed);
    return deferred.promise;
  };

}

export default {
  name: 'SwaggerOAuthService',
  fn: SwaggerOAuthService
};
