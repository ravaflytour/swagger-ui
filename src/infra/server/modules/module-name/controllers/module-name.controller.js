import exampleRepository from '../repositories/example.repository';

export function getAll(req, res) {
  res.json(exampleRepository.getAll(req.params));
}

export function getById(req, res) {
  res.json(exampleRepository.getById(req.params.id));
}

export function post(req, res) {
  res.json(exampleRepository.create(req.body));
}

export function put(req, res) {
  res.json(exampleRepository.update(req.body));
}

export function del(req, res) {
  res.json(exampleRepository.del(req.params.id));
}

export default {
  getAll: getAll,
  getById: getById,
  post: post,
  put: put,
  del: del
};
