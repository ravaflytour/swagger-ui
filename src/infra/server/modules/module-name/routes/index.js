import router from '../../../web-app/router';
import controller from '../controllers/module-name.controller';

router.get('/example', controller.getAll);
router.get('/example/:id', controller.getById);
router.post('/example', controller.post);
router.put('/example/:id', controller.put);
router.delete('/example/:id', controller.del);

export default router;

