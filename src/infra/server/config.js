export default {
  cookieSecret: process.env.COOKIE_SECRET,
  cookieKey: process.env.COOKIE_KEY,
  redisHost: process.env.REDIS_HOST,
  redisPort: process.env.REDIS_PORT,
  redisPassword: process.env.REDIS_PASSWORD,
}
