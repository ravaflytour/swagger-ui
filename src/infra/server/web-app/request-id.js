import cuid from'cuid';

export default function reqId(req, res, next) {
  req.id = cuid();
  res.headers = res.headers || {};
  res.headers['X-Request-Id'] = req.id;
  next();
};
