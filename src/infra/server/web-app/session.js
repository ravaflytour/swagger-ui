import expressSession from 'express-session';
import connectRedis from 'connect-redis';
import config from '../config';

const RedisStore = connectRedis(expressSession);

const redisConfig   = {
  host: config.redisHost,
  port: config.redisPort,
  pass: config.redisPassword,
  ttl : config.redisTTL || 1200 // Measured in seconds: 20 minutes
};

export default expressSession({
  store : new RedisStore(redisConfig),
  secret: config.cookieSecret,
  //key   : config.cookieKey,
  resave: false,
  saveUninitialized: true
});
