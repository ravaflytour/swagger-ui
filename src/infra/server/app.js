import express from 'express';
import bodyParser from 'body-parser';
import compression from 'compression';
import cors from 'cors';
import helmet from 'helmet';
import path from 'path';
import reqId from './web-app/request-id';
import session from './web-app/session';
import routes from './routes';


const app = express();
const port = process.env.PORT || 3000;
const staticDir = path.join(__dirname, '../../build/client');

app.enable('strict routing');
app.enable('case sensitive routing');
app.enable('trust proxy');
app.disable('x-powered-by');

app.use(helmet())
app.use(session);
app.use(reqId);
app.use(compression());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.use(cors());

const staticHandler = express.static(staticDir);

app.use('/', routes);
app.use('/', staticHandler);

app.listen(port);
console.log('Listen on port ' + port);

export default app;
